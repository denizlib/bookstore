package com.denizlib.bookstore.dto;

import com.denizlib.bookstore.entity.Book;
import com.denizlib.bookstore.entity.Bookstore;
import com.denizlib.bookstore.entity.BookstoreBook;
import com.denizlib.bookstore.entity.Category;

public class BookstoreBookDTO {

    private Long bookId;
    private Double price;
    private String bookName;
    private Category category;

    public BookstoreBookDTO(){

    }

    public BookstoreBookDTO(BookstoreBook bookstoreBook){
        bookId = bookstoreBook.getBook().getId();
        price = bookstoreBook.getBookPrice();
        bookName = bookstoreBook.getBook().getName();
        category = bookstoreBook.getBook().getCategory();
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
