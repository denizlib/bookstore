package com.denizlib.bookstore.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;

@Entity(name = "BookstoreBook")
@Table(name = "bookstore_books")
@AssociationOverrides({
        @AssociationOverride(name = "pk.book",
                joinColumns = @JoinColumn(name = "book_id")),
        @AssociationOverride(name = "pk.bookstore",
                joinColumns = @JoinColumn(name = "bookstore_id"))})
public class BookstoreBook {

    private BookstoreBookId pk = new BookstoreBookId();

    private Double bookPrice;

    public BookstoreBook(){

    }

    public BookstoreBook(Bookstore bookstore, Book book, Double price){
        setBookstore(bookstore);
        setBook(book);
        setBookPrice(price);
    }

    public BookstoreBook(Bookstore bookstore, Book book){
        setBookstore(bookstore);
        setBook(book);
    }

    public BookstoreBook(Long bookstoreId, Long bookId){
        setBookstore(new Bookstore(){{setId(bookstoreId);}});
        setBook(new Book(){{setId(bookId);}});
    }

    @EmbeddedId
    private BookstoreBookId getPk() {
        return pk;
    }

    private void setPk(BookstoreBookId bookstoreBookId) {
        this.pk = bookstoreBookId;
    }

    @Transient
    public Book getBook() {
        return this.pk.getBook();
    }

    public void setBook(Book book) {
        getPk().setBook(book);
    }

    @Transient
    public Bookstore getBookstore() {
        return this.pk.getBookstore();
    }

    public void setBookstore(Bookstore bookstore) {
        getPk().setBookstore(bookstore);
    }

    public Double getBookPrice() {
        return this.bookPrice;
    }

    public void setBookPrice(Double bookPrice) {
        this.bookPrice = bookPrice;
    }

}
