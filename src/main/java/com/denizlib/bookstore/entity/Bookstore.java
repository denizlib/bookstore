package com.denizlib.bookstore.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Bookstore {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "city", unique = true)
    private String city;

    @JsonIgnore
    @OneToMany(mappedBy = "pk.bookstore", fetch = FetchType.LAZY)
    private Set<BookstoreBook> books = new HashSet<>();

    public Bookstore(){

    }

    public Bookstore(Long id){
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Set<BookstoreBook> getBooks() {
        return books;
    }

    public void setBooks(Set<BookstoreBook> books) {
        this.books = books;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        Bookstore bs = (Bookstore) o;
        return Objects.equals(id, bs.getId());
    }
}
