package com.denizlib.bookstore.endpoint;

import com.denizlib.bookstore.entity.Category;
import com.denizlib.bookstore.service.BookService;
import com.denizlib.bookstore.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CategoryController {

    private final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    private final CategoryService categoryService;

    private final BookService bookService;

    public CategoryController(CategoryService categoryService, BookService bookService) {
        this.categoryService = categoryService;
        this.bookService = bookService;
    }

    @GetMapping("/categories")
    public ResponseEntity<List<Category>> getAllCategories() {
        try {
            return new ResponseEntity<>(categoryService.getAllCategories(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PostMapping("/categories")
    public ResponseEntity<Category> create(Category category) {
        if (category.getId() != null) {
            logger.error("New category cannot already have an id!");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(categoryService.save(category), HttpStatus.CREATED);

    }

    @PutMapping("/categories")
    public ResponseEntity<Category> update(Category category) {
        if (category.getId() == null) {
            logger.error("Invalid category id!");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(categoryService.save(category), HttpStatus.OK);
    }
}
