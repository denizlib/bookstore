package com.denizlib.bookstore.endpoint;

import com.denizlib.bookstore.entity.Book;
import com.denizlib.bookstore.entity.BookstoreBook;
import com.denizlib.bookstore.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class BookController {

    private final Logger logger = LoggerFactory.getLogger(BookController.class);

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/books")
    public ResponseEntity<List<Book>> getAllBooks(){
        return new ResponseEntity<>(bookService.getAllBooks(), HttpStatus.OK);
    }

    @PostMapping("/books")
    public ResponseEntity<Book> create(Book book){
        if (book.getId() != null) {
            logger.error("New book cannot already have an id!");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(bookService.save(book), HttpStatus.CREATED);
    }

    @PutMapping("/books")
    public ResponseEntity<Book> update(Book book){
        if (book.getId() == null) {
            logger.error("Invalid book id!");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(bookService.save(book), HttpStatus.OK);
    }

    @GetMapping("/categories/{name}/books")
    public ResponseEntity<List<Book>> getBooksByCategory(@PathVariable("name") String categoryName) {
        if (StringUtils.isEmpty(categoryName)) {
            logger.error("Invalid request!");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(bookService.getBooksByCategoryName(categoryName), HttpStatus.OK);
    }
}
