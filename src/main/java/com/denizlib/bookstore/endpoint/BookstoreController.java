package com.denizlib.bookstore.endpoint;

import com.denizlib.bookstore.dto.BookstoreBookDTO;
import com.denizlib.bookstore.entity.Bookstore;
import com.denizlib.bookstore.entity.BookstoreBook;
import com.denizlib.bookstore.entity.Category;
import com.denizlib.bookstore.service.BookService;
import com.denizlib.bookstore.service.BookstoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class BookstoreController {

    private Logger logger = LoggerFactory.getLogger(BookstoreController.class);

    private final BookstoreService bookstoreService;

    private final BookService bookService;

    public BookstoreController(BookstoreService bookstoreService, BookService bookService) {
        this.bookstoreService = bookstoreService;
        this.bookService = bookService;
    }

    @GetMapping("/bookstores")
    public ResponseEntity<List<Bookstore>> getAllBookstores() {
        return new ResponseEntity<>(bookstoreService.getAllBookstores(), HttpStatus.OK);
    }

    @PostMapping("/bookstores")
    public ResponseEntity<Bookstore> create(Bookstore bookstore) {
        if (bookstore.getId() != null) {
            logger.error("New bookstore cannot already have an id!");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(bookstoreService.save(bookstore), HttpStatus.CREATED);
    }

    @PutMapping("/bookstores")
    public ResponseEntity<Bookstore> update(Bookstore bookstore) {
        if (bookstore.getId() == null) {
            logger.error("Invalid bookstore id!");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(bookstoreService.save(bookstore), HttpStatus.OK);
    }

    @GetMapping("/bookstores/{city}/books")
    public ResponseEntity<List<BookstoreBookDTO>> getBooksByBookstore(@PathVariable("city") String city) {
        if (StringUtils.isEmpty(city)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(bookService.getBooksByBookstoreCity(city), HttpStatus.OK);
    }

    @GetMapping("/bookstores/{bookstoreId}/books")
    public ResponseEntity<List<BookstoreBookDTO>> getBooksByBookstore(@PathVariable("bookstoreId") Long bookstoreId) {
        if (StringUtils.isEmpty(bookstoreId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(bookService.getBooksByBookstore(new Bookstore(bookstoreId)), HttpStatus.OK);
    }

    @PutMapping("/bookstores/{bookstoreId}/books/{bookId}")
    public ResponseEntity<BookstoreBook> addBook(@PathVariable("bookId") Long bookId, @PathVariable("bookstoreId") Long bookstoreId, Double price) {
        if (bookId == null || bookstoreId == null || price == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(bookstoreService.addBook(bookstoreId, bookId, price), HttpStatus.CREATED);
    }

    @DeleteMapping("/bookstores/{bookstoreId}/books/{bookId}")
    public ResponseEntity addBook(@PathVariable("bookId") Long bookId, @PathVariable("bookstoreId") Long bookstoreId) {
        if (bookId == null || bookstoreId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        bookstoreService.removeBook(bookstoreId, bookId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/bookstores/{bookstoreId}/categories/{categoryId}/books")
    public ResponseEntity<List<BookstoreBookDTO>> getBooksByBookstoreAndCategory(@PathVariable("bookstoreId") Long bookstoreId, @PathVariable("categoryId") Long categoryId) {
        if (StringUtils.isEmpty(categoryId) || StringUtils.isEmpty(bookstoreId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(bookService.getBooksByBookstoreAndCategory(new Bookstore(bookstoreId), new Category(categoryId)), HttpStatus.OK);
    }
}
