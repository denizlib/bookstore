package com.denizlib.bookstore.repository;

import com.denizlib.bookstore.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookstoreBookRepository extends JpaRepository<BookstoreBook, BookstoreBookId> {

    @Query("select bsb from BookstoreBook bsb where bsb.pk.bookstore.city = :city")
    List<BookstoreBook> getByBookstoreCity(@Param("city") String city);

    @Query("select bsb from BookstoreBook bsb where bsb.pk.bookstore = :bookstore")
    List<BookstoreBook> getByBookstore(@Param("bookstore") Bookstore bookstore);

    @Query("select bsb from BookstoreBook bsb where bsb.pk.bookstore = :bookstore and bsb.pk.book.category = :category")
    List<BookstoreBook> getByBookstoreAndBookCategory(@Param("bookstore") Bookstore bookstore, @Param("category") Category category);

}
