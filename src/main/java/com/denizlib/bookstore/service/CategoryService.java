package com.denizlib.bookstore.service;

import com.denizlib.bookstore.entity.Category;

import java.util.List;

public interface CategoryService {

    public Category save(Category category);

    public List<Category> getAllCategories();
}
