package com.denizlib.bookstore.service;

import com.denizlib.bookstore.dto.BookstoreBookDTO;
import com.denizlib.bookstore.entity.Book;
import com.denizlib.bookstore.entity.Bookstore;
import com.denizlib.bookstore.entity.Category;

import java.util.List;

public interface BookService {

    public Book save(Book book);

    public List<Book> getAllBooks();

    public List<Book> getBooksByCategoryName(String categoryName);

    public List<BookstoreBookDTO> getBooksByBookstore(Bookstore bookstore);

    public List<BookstoreBookDTO> getBooksByBookstoreCity(String city);

    public List<BookstoreBookDTO> getBooksByBookstoreAndCategory(Bookstore bookstore, Category category);

}
