package com.denizlib.bookstore.service;

import com.denizlib.bookstore.entity.Book;
import com.denizlib.bookstore.entity.Bookstore;
import com.denizlib.bookstore.entity.BookstoreBook;

import java.util.List;

public interface BookstoreService {

    public Bookstore save(Bookstore bookstore);

    public List<Bookstore> getAllBookstores();

    public BookstoreBook addBook(Long bookstoreId, Long bookId, Double price);

    public void removeBook(Long bookstoreId, Long bookId);
}
