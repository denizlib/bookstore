package com.denizlib.bookstore.service.implementation;

import com.denizlib.bookstore.dto.BookstoreBookDTO;
import com.denizlib.bookstore.entity.Book;
import com.denizlib.bookstore.entity.Bookstore;
import com.denizlib.bookstore.entity.BookstoreBook;
import com.denizlib.bookstore.entity.Category;
import com.denizlib.bookstore.repository.BookRepository;
import com.denizlib.bookstore.repository.BookstoreBookRepository;
import com.denizlib.bookstore.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private Logger logger = LoggerFactory.getLogger(BookServiceImpl.class);

    private final BookRepository bookRepository;

    private final BookstoreBookRepository bookstoreBookRepository;

    public BookServiceImpl(BookRepository bookRepository, BookstoreBookRepository bookstoreBookRepository) {
        this.bookRepository = bookRepository;
        this.bookstoreBookRepository = bookstoreBookRepository;
    }

    /**
     * Save a book
     *
     * @param book entity to save
     * @return the persistent entity
     */
    @Override
    public Book save(Book book) {
        logger.debug("Saving book: {}", book.getName());
        return bookRepository.save(book);
    }

    /**
     * Get all the books.
     *
     * @return the list of entities
     */
    @Override
    public List<Book> getAllBooks() {
        logger.debug("Fetching all book entries.");
        return bookRepository.findAll();
    }

    /**
     * Get books by Category name.
     *
     * @param categoryName name of the Category
     * @return the entities
     */
    @Override
    public List<Book> getBooksByCategoryName(String categoryName) {
        logger.debug("Fetching books by category name: {}", categoryName);
        return bookRepository.getBooksByCategory_Name(categoryName);
    }

    /**
     * Get books by Bookstore.
     *
     * @param bookstore Bookstore object
     * @return the entities
     */
    @Override
    public List<BookstoreBookDTO> getBooksByBookstore(Bookstore bookstore) {
        logger.debug("Fetching books by Bookstore: {}", bookstore);
        List<BookstoreBookDTO> books = new ArrayList<>();
        for (BookstoreBook bookstoreBook : bookstoreBookRepository.getByBookstore(bookstore)) {
            books.add(new BookstoreBookDTO(bookstoreBook));
        }
        return books;
    }

    /**
     * Get books by Bookstore.
     *
     * @param city String bookstoreCity
     * @return the entities
     */
    @Override
    public List<BookstoreBookDTO> getBooksByBookstoreCity(String city) {
        logger.debug("Fetching books by Bookstore City: {}", city);
        List<BookstoreBookDTO> books = new ArrayList<>();
        for (BookstoreBook bookstoreBook : bookstoreBookRepository.getByBookstoreCity(city)) {
            books.add(new BookstoreBookDTO(bookstoreBook));
        }
        return books;
    }

    /**
     * Get books by Bookstore and Category.
     *
     * @param bookstore String bookstore city
     * @param category Category book category
     * @return the entities
     */
    @Override
    public List<BookstoreBookDTO> getBooksByBookstoreAndCategory(Bookstore bookstore, Category category) {
        logger.debug("Fetching books by bookstore: {} and category: {}", bookstore.getCity(), category.getName());
        List<BookstoreBookDTO> books = new ArrayList<>();
        for (BookstoreBook bookstoreBook : bookstoreBookRepository.getByBookstoreAndBookCategory(bookstore, category)) {
            books.add(new BookstoreBookDTO(bookstoreBook));
        }
        return books;
    }


}
