package com.denizlib.bookstore.service.implementation;

import com.denizlib.bookstore.entity.Book;
import com.denizlib.bookstore.entity.Bookstore;
import com.denizlib.bookstore.entity.BookstoreBook;
import com.denizlib.bookstore.repository.BookstoreBookRepository;
import com.denizlib.bookstore.repository.BookstoreRepository;
import com.denizlib.bookstore.service.BookstoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookstoreServiceImpl implements BookstoreService {

    private Logger logger = LoggerFactory.getLogger(BookstoreServiceImpl.class);

    private final BookstoreRepository bookstoreRepository;

    private final BookstoreBookRepository bookstoreBookRepository;

    public BookstoreServiceImpl(BookstoreRepository bookstoreRepository, BookstoreBookRepository bookstoreBookRepository) {
        this.bookstoreRepository = bookstoreRepository;
        this.bookstoreBookRepository = bookstoreBookRepository;
    }

    @Override
    public Bookstore save(Bookstore bookstore) {
        logger.debug("Saving bookstore: {}", bookstore.getCity());
        return bookstoreRepository.save(bookstore);
    }

    @Override
    public List<Bookstore> getAllBookstores() {
        logger.debug("Fetching all bookstores.");
        return bookstoreRepository.findAll();
    }

    @Override
    public BookstoreBook addBook(Long bookstoreId, Long bookId, Double price) {
        BookstoreBook bookstoreBook = new BookstoreBook(bookstoreId, bookId);
        bookstoreBook.setBookPrice(price);
        return bookstoreBookRepository.save(bookstoreBook);
    }

    @Override
    public void removeBook(Long bookstoreId, Long bookId) {
        logger.debug("Removing Book: {} from Bookstore: {}", bookId, bookstoreId);
        bookstoreBookRepository.delete(new BookstoreBook(bookstoreId, bookId));
    }
}
