package com.denizlib.bookstore.service.implementation;

import com.denizlib.bookstore.dto.BookstoreBookDTO;
import com.denizlib.bookstore.entity.Book;
import com.denizlib.bookstore.entity.Bookstore;
import com.denizlib.bookstore.entity.BookstoreBook;
import com.denizlib.bookstore.entity.Category;
import com.denizlib.bookstore.repository.BookRepository;
import com.denizlib.bookstore.repository.BookstoreBookRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class BookServiceImplTest {

    @InjectMocks
    BookServiceImpl bookService;

    @Mock
    BookRepository bookRepository;

    @Mock
    BookstoreBookRepository bookstoreBookRepository;

    @Before
    public void setUp(){
        Bookstore bookstore = new Bookstore();
        bookstore.setCity("Istanbul");
        bookstore.setId(1L);

        Bookstore bookstore2 = new Bookstore();
        bookstore2.setCity("Ankara");
        bookstore2.setId(2L);

        Category category = new Category();
        category.setId(11L);
        category.setName("Novel");

        Category category2 = new Category();
        category2.setId(12L);
        category2.setName("Short Story");

        Book book = new Book();
        book.setName("Test Book");
        book.setCategory(category);

        Book book2 = new Book();
        book2.setName("Test Book 2");
        book2.setCategory(category2);

        BookstoreBook bookstoreBook = new BookstoreBook();
        bookstoreBook.setBook(book);
        bookstoreBook.setBookstore(bookstore);
        bookstoreBook.setBookPrice(10.0);

        BookstoreBook bookstoreBook2 = new BookstoreBook();
        bookstoreBook2.setBook(book2);
        bookstoreBook2.setBookstore(bookstore2);
        bookstoreBook2.setBookPrice(15.0);

        BookstoreBook bookstoreBook3 = new BookstoreBook();
        bookstoreBook3.setBook(book2);
        bookstoreBook3.setBookstore(bookstore);
        bookstoreBook3.setBookPrice(12.5);

        Mockito.when(bookRepository.findAll()).thenReturn(Arrays.asList(book,book2));
        Mockito.when(bookRepository.getBooksByCategory_Name("Novel")).thenReturn(Collections.singletonList(book));
        Mockito.when(bookstoreBookRepository.getByBookstore(ArgumentMatchers.eq(bookstore))).thenReturn(Arrays.asList(bookstoreBook,bookstoreBook3));
        Mockito.when(bookstoreBookRepository.getByBookstoreAndBookCategory(ArgumentMatchers.eq(bookstore), ArgumentMatchers.eq(category))).thenReturn(Collections.singletonList(bookstoreBook));
    }

    @Test
    public void save() {
        Book bookToSave = new Book();
        bookToSave.setName("Saved Book");
        Mockito.when(bookRepository.save(ArgumentMatchers.any(Book.class))).thenReturn(bookToSave);
        Book saved = bookService.save(bookToSave);
        assertEquals(bookToSave.getName(), saved.getName());
    }

    @Test
    public void getAllBooks() {
        List<Book> searchedList = bookService.getAllBooks();
        assertEquals(2,searchedList.size());
    }

    @Test
    public void getBooksByCategoryName() {
        List<Book> searchedList = bookService.getBooksByCategoryName("Novel");
        assertEquals(1,searchedList.size());
    }

    @Test
    public void getBooksByBookstore() {
        List<BookstoreBookDTO> searchedList = bookService.getBooksByBookstore(new Bookstore(1L));
        assertEquals(2, searchedList.size());
    }

    @Test
    public void getBooksByBookstoreAndCategory() {
        List<BookstoreBookDTO> searchedList = bookService.getBooksByBookstoreAndCategory(new Bookstore(1L), new Category(11L));
        assertEquals(1, searchedList.size());
    }
}