package com.denizlib.bookstore.service.implementation;

import com.denizlib.bookstore.entity.Book;
import com.denizlib.bookstore.entity.Bookstore;
import com.denizlib.bookstore.entity.BookstoreBook;
import com.denizlib.bookstore.repository.BookstoreBookRepository;
import com.denizlib.bookstore.repository.BookstoreRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class BookstoreServiceImplTest {

    @InjectMocks
    BookstoreServiceImpl bookstoreService;

    @Mock
    BookstoreRepository bookstoreRepository;

    @Mock
    BookstoreBookRepository bookstoreBookRepository;

    @Test
    public void save() {
        Bookstore bookstoreToSave = new Bookstore();
        bookstoreToSave.setCity("Saved Book");
        Mockito.when(bookstoreRepository.save(ArgumentMatchers.any(Bookstore.class))).thenReturn(bookstoreToSave);
        Bookstore saved = bookstoreService.save(bookstoreToSave);
        assertEquals(bookstoreToSave.getCity(), saved.getCity());
    }

    @Test
    public void getAllBookstores() {
        Mockito.when(bookstoreRepository.findAll()).thenReturn(Arrays.asList(new Bookstore(), new Bookstore(), new Bookstore()));
        List<Bookstore> searchedList = bookstoreService.getAllBookstores();
        assertEquals(3, searchedList.size());
    }

    @Test
    public void addBook() {
        Book book = new Book();
        book.setName("Test Book");
        Bookstore bookstore = new Bookstore();
        bookstore.setCity("Istanbul");
        BookstoreBook bookstoreBook = new BookstoreBook(bookstore, book, 30.0);
        Mockito.when(bookstoreBookRepository.save(ArgumentMatchers.any(BookstoreBook.class))).thenReturn(bookstoreBook);
        BookstoreBook saved = bookstoreService.addBook(1L, 2L, 30.0);
        assertEquals(book.getName(), saved.getBook().getName());
        assertEquals(bookstore.getCity(), saved.getBookstore().getCity());
        assertEquals(Double.valueOf(30), saved.getBookPrice());
    }
}