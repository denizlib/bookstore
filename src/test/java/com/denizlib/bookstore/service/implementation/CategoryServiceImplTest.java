package com.denizlib.bookstore.service.implementation;

import com.denizlib.bookstore.entity.Category;
import com.denizlib.bookstore.repository.CategoryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CategoryServiceImplTest {

    @InjectMocks
    CategoryServiceImpl categoryService;

    @Mock
    CategoryRepository categoryRepository;

    @Test
    public void save() {
        Category toSave = new Category();
        toSave.setName("Novel");
        Mockito.when(categoryRepository.save(ArgumentMatchers.any(Category.class))).thenReturn(toSave);
        Category saved = categoryService.save(toSave);
        assertEquals(toSave.getName(), saved.getName());
    }

    @Test
    public void getAllCategories() {
        Mockito.when(categoryRepository.findAll()).thenReturn(Arrays.asList(new Category(), new Category(), new Category()));
        List<Category> searchedList = categoryService.getAllCategories();
        assertEquals(3, searchedList.size());
    }
}