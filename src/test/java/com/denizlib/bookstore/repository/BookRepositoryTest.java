package com.denizlib.bookstore.repository;

import com.denizlib.bookstore.entity.Book;
import com.denizlib.bookstore.entity.Category;
import org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(BeforeAfterSpringTestRunner.class)
public class BookRepositoryTest extends BaseIntegrationTestClass {

    @Autowired
    BookRepository bookRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Test
    public void getBooksByCategoryName(){
        Category category = new Category();
        category.setName("Novel");
        category = categoryRepository.save(category);

        Book book1 = new Book();
        book1.setName("test1");
        book1.setCategory(category);
        bookRepository.save(book1);

        Book book2 = new Book();
        book2.setName("test2");
        book2.setCategory(category);
        bookRepository.save(book2);

        List<Book> books = bookRepository.getBooksByCategory_Name("Novel");
        assertEquals(2, books.size());
    }

}