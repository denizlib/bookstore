package com.denizlib.bookstore.repository;

import com.denizlib.bookstore.entity.Book;
import com.denizlib.bookstore.entity.Bookstore;
import com.denizlib.bookstore.entity.BookstoreBook;
import com.denizlib.bookstore.entity.Category;
import org.bitbucket.radistao.test.annotation.BeforeAllMethods;
import org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(BeforeAfterSpringTestRunner.class)
public class BookstoreBookRepositoryTest extends BaseIntegrationTestClass {

    @Autowired
    BookRepository bookRepository;

    @Autowired
    BookstoreRepository bookstoreRepository;

    @Autowired
    BookstoreBookRepository bookstoreBookRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @BeforeAllMethods
    public void setUp(){
        Category category = new Category();
        category.setName("short");
        category = categoryRepository.saveAndFlush(category);

        Category category2 = new Category();
        category2.setName("novel");
        category2 = categoryRepository.saveAndFlush(category2);

        Bookstore bookstore = new Bookstore();
        bookstore.setCity("test city 1");
        bookstore = bookstoreRepository.saveAndFlush(bookstore);

        Bookstore bookstore2 = new Bookstore();
        bookstore2.setCity("test city 2");
        bookstore2 = bookstoreRepository.saveAndFlush(bookstore2);

        Book book1 = new Book();
        book1.setName("Test Book 1");
        book1.setCategory(category);
        book1 = bookRepository.saveAndFlush(book1);

        Book book2 = new Book();
        book2.setName("Test Book 2");
        book2.setCategory(category2);
        book2 = bookRepository.saveAndFlush(book2);

        bookstoreBookRepository.saveAndFlush(new BookstoreBook(bookstore, book1, 10.0));
        bookstoreBookRepository.saveAndFlush(new BookstoreBook(bookstore, book2, 12.0));
        bookstoreBookRepository.saveAndFlush(new BookstoreBook(bookstore2, book2, 13.0));
    }

    @Test
    public void getByBookstoreCity() {
        List<BookstoreBook> bookstoreBooks = bookstoreBookRepository.getByBookstoreCity("test city 1");
        assertEquals(2, bookstoreBooks.size());
    }

    @Test
    public void getByBookstore() {
        List<BookstoreBook> bookstoreBooks = bookstoreBookRepository.getByBookstore(new Bookstore(2L));
        assertEquals(1, bookstoreBooks.size());
    }

    @Test
    public void getByBookstoreAndBookCategory() {
        List<BookstoreBook> bookstoreBooks = bookstoreBookRepository.getByBookstoreAndBookCategory(new Bookstore(1L),new Category(2L));
        assertEquals(1, bookstoreBooks.size());
    }

}