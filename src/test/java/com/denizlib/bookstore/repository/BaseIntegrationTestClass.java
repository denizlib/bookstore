package com.denizlib.bookstore.repository;

import org.springframework.test.context.TestPropertySource;

@TestPropertySource(locations = "classpath:application-test.properties")
public class BaseIntegrationTestClass {
}
